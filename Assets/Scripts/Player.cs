﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public Image barraVida;
    public Text textoItems;
    float vida;
    public int items;
    public int maxItems;


    // Start is called before the first frame update
    void Start()
    {
        vida = 100;
        barraVida.fillAmount = 1;

        textoItems.text = "0 de " + maxItems.ToString();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RecibirGolpe()
    {
        vida = vida - 15;
        barraVida.fillAmount = vida / 100;

        if (vida <= 0)
            SceneManager.LoadScene(1);

    }


    public void ItemConseguido()
    {
        items ++;

        textoItems.text = items.ToString() + " de " + maxItems.ToString() ;

        if (items == maxItems)
            SceneManager.LoadScene(1);
    }
}
